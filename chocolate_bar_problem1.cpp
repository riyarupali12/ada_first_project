#include <iostream>
using namespace std;
const int MAX=50;
const int MIN=9999;

void breaks(int a, int b,int c[MAX][MAX]);
int main()
{
	int a,b,c[MAX][MAX],i,j;
	cout<<"enter the rows and columns of chocolate bar:";
    cin >> a >> b;
    for(int i=1;i<MAX;i++)
	{
        for(int j=1;j<MAX;j++)
		{
            if(i==1 && j==1) 
				c[i][j]=0;
			else
            	breaks(i,j,c);
        }
    }
    cout<<"minimum no of breaks:"<<endl;
    cout << c[a][b] << endl;
    
    
}
void breaks(int a, int b,int c[MAX][MAX])
{
    int minimum=MIN;
    for(int k=1;k<a;k++)
	{
        minimum=min(minimum,c[k][b]+c[a-k][b]+1);                                         
    }
    for(int k=1;k<b;k++)
	{
        minimum=min(minimum,c[a][k]+c[a][b-k]+1);
    }
    c[a][b]=minimum;
}